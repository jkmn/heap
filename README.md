# heap

This is an implementation of a binary heap data structure I wrote after being
asked about heaps in an interview. I used
https://en.wikipedia.org/wiki/Heap_(data_structure) as reference. I don't ever
expect to use this in production (and neither should you). This is only a coding
exercise.

For onlookers, I acknowledge that this is not thread-safe and there are a lot of
finer CompSci points that could be discussed. For example, I suspect that this
code does not guarantee that the binary tree will always be balanced,
particularly if the little sorts are removed.

This particular implementation assumes that the highest priority value is the
root. If I ever get the itch, I'd like to compare this implementation with
another that assumes the lowest priority is inserted at the root.