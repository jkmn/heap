package heap_test

import (
	"github.com/stretchr/testify/assert"
	"heap"
	"testing"
	"time"
)

func TestNode_Descendants(t *testing.T) {
	type TestCase struct {
		Name string
		Node *heap.Node
		Size int
	}

	testCases := []TestCase{
		{
			Name: "Nil Node",
			Node: nil,
			Size: 0,
		},
		{
			Name: "Single Node",
			Node: &heap.Node{
				Stamp: time.Now(),
				Value: "coconut",
			},
			Size: 1,
		},
		{
			Name: "Imbalanced Right",
			Node: &heap.Node{
				Stamp: time.Now(),
				Value: "coconut",
				Right: &heap.Node{
					Stamp: time.Now().Add(time.Second),
					Value: "banana",
				},
			},
			Size: 2,
		},
		{
			Name: "Imbalanced Left",
			Node: &heap.Node{
				Stamp: time.Now(),
				Value: "coconut",
				Left: &heap.Node{
					Stamp: time.Now().Add(time.Second),
					Value: "grape",
				},
			},
			Size: 2,
		},
		{
			Name: "Three Balanced",
			Node: &heap.Node{
				Stamp: time.Now(),
				Value: "coconut",
				Left: &heap.Node{
					Stamp: time.Now().Add(time.Second),
					Value: "grape",
				},
				Right: &heap.Node{
					Stamp: time.Now().Add(2 * time.Second),
					Value: "banana",
				},
			},
			Size: 3,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			a := assert.New(t)
			qty := tc.Node.Size()
			a.Equal(tc.Size, qty)
		})
	}
}

func TestNewHeap(t *testing.T) {
	type TestCase struct {
		Name   string
		Values []string
		Size   int
	}

	testCases := []TestCase{
		{
			Name:   "Nil Values",
			Values: nil,
			Size:   0,
		},
		{
			Name:   "Zero Values",
			Values: []string{},
			Size:   0,
		},
		{
			Name:   "One Value",
			Values: []string{"coconut"},
			Size:   1,
		},
		{
			Name: "Four Values",
			Values: []string{
				"one",
				"two",
				"three",
				"four",
			},
			Size: 4,
		},
		{
			Name: "Fifty Values",
			Values: []string{
				"0",
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9",
				"10",
				"11",
				"12",
				"13",
				"14",
				"15",
				"16",
				"17",
				"18",
				"19",
				"20",
				"21",
				"22",
				"23",
				"24",
				"25",
				"26",
				"27",
				"28",
				"29",
				"30",
				"31",
				"32",
				"33",
				"34",
				"35",
				"36",
				"37",
				"38",
				"39",
				"40",
				"41",
				"42",
				"43",
				"44",
				"45",
				"46",
				"47",
				"48",
				"49",
			},
			Size: 50,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			a := assert.New(t)
			h := heap.NewHeap(tc.Values)
			a.Equal(tc.Size, h.Size())

			// We should be able to retrieve the values in the same order they
			// were inserted.

			for i, v := range tc.Values {
				a.Equal(tc.Size-i, h.Size())
				v_ := h.Pop()
				a.NotNil(v_)
				if v_ == nil {
					break
				}

				h.Root.CheckBalance()

				a.Equal(v, *v_)
			}
		})
	}
}

func TestMerge(t *testing.T) {

	a := assert.New(t)

	expected := []string{
		"a",
		"b",
		"c",
		"d",
	}

	now := time.Now()

	heap0 := heap.Heap{
		Root: &heap.Node{
			Stamp: now,
			Value: "a",
			Left: &heap.Node{
				Stamp: now.Add(2 * time.Second),
				Value: "c",
			},
			Right: nil,
		},
	}

	heap1 := heap.Heap{
		Root: &heap.Node{
			Stamp: now.Add(time.Second),
			Value: "b",
			Left: &heap.Node{
				Stamp: now.Add(3 * time.Second),
				Value: "d",
			},
			Right: nil,
		},
	}

	heap2 := heap.Merge(heap0, heap1)

	actual := make([]string, 0)
	for heap2.Size() > 0 {
		v := heap2.Pop()
		actual = append(actual, *v)
	}

	a.Equal(len(expected), len(actual))
	a.Equal(expected, actual)
}
