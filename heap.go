package heap

import "time"

// Heap is an implementation of the heap data structure as defined by Wikipedia
// as of 2 Jan 2022. A zero-initialized Heap is a valid heap with a zero size
// and no contents.
//
// Technically speaking, there isn't a huge reason to have separate Heap and
// Node types. I like having useful zero-value types, however, and the Heap type
// makes this easier.
type Heap struct {
	Root *Node
}

func NewHeap(values []string) Heap {

	heap := Heap{}
	for _, v := range values {
		heap.Insert(v)
	}

	return heap
}

func (h *Heap) Insert(value string) {
	// Creating the new node up-front makes insertion much DRY-er. No matter
	// what, this node is going to be allocated on the stack. It's just a matter
	// of where it's connected to the tree.
	node := &Node{
		Stamp: time.Now(),
		Value: value,
	}

	if h.Root == nil {
		h.Root = node
		return
	}

	h.Root.Append(node)
}

func (h Heap) Size() int {
	return h.Root.Size()
}

func (h *Heap) Peek() *string {
	if h.Root == nil {
		return nil
	}

	return &h.Root.Value
}

func (h *Heap) Pop() *string {
	if h.Root == nil {
		return nil
	}

	// This is the value to return when we're done with re-balancing.
	out := &h.Root.Value

	// If both children are nil, then set root to nil and return.
	if h.Root.Left == nil && h.Root.Right == nil {
		h.Root = nil
		return out
	}

	// If either child is nil, we can just promote the non-nil child.
	if h.Root.Left == nil {
		h.Root = h.Root.Right
		return out
	}
	if h.Root.Right == nil {
		h.Root = h.Root.Left
		return out
	}

	// We now have two nodes that need to go someplace.
	// Let's take the youngest node and sift it down.

	var (
		a *Node = h.Root.Left
		b *Node = h.Root.Right
	)

	h.Root = a
	h.Root.SiftDown(b)

	return out
}

func Merge(a, b Heap) Heap {
	out := Heap{}
	for a.Size() != 0 || b.Size() != 0 {
		if a.Size() == 0 {
			v := b.Pop()
			out.Insert(*v)
			continue
		}
		if b.Size() == 0 {
			v := a.Pop()
			out.Insert(*v)
			continue
		}

		if a.Root.Stamp.Before(b.Root.Stamp) {
			v := a.Pop()
			out.Insert(*v)
		} else {
			v := b.Pop()
			out.Insert(*v)
		}
	}

	return out
}
