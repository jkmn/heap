package heap

import (
	"log"
	"time"
)

type Node struct {
	Stamp time.Time
	Value string
	Left  *Node
	Right *Node
}

func (n *Node) Append(node *Node) {

	// First, check to see if we have a nil child that we can use.
	if n.Left == nil {
		n.Left = node
		return
	}

	if n.Right == nil {
		n.Right = node
		return
	}

	// This is our fall-through case. We will check the size of each child to
	// see which branch of the tree is optimal.

	if n.Left.Size() <= n.Right.Size() {
		n.Left.Append(node)
	} else {
		n.Right.Append(node)
	}
}

func (n *Node) Size() int {
	if n == nil {
		return 0
	}

	// The current node is not nil, therefore we should include the node itself.
	var sum int = 1

	// Include the sizes of the children.
	sum += n.Left.Size()
	sum += n.Right.Size()

	return sum
}

func (n *Node) SiftDown(sifted *Node) {

	// First, let's address the most convenient case, that we have a nil child.
	if n.Left == nil {
		n.Left = sifted
		return
	}
	if n.Right == nil {
		// Maintain order.
		n.Right = n.Left
		n.Left = sifted
		return
	}

	// Which child is youngest? That node will be replaced by the incoming
	// sifted node. That youngest child will be sifted downward.

	// These are our nodes. They're not yet sorted, but we can assume the
	// incoming node is the oldest.
	a := sifted
	b := n.Left
	c := n.Right

	// If the existing children are out of order, swap them.
	// If everything was coded correctly, these nodes should *never* be out of order.
	if c.Stamp.Before(b.Stamp) {
		b, c = c, b
	}

	// We now have three nodes, ordered from oldest to youngest.
	// The youngest will be sifted downward.

	// Re-attach the oldest children to the parent.
	n.Left = a
	n.Right = b

	// Now, lets sift the youngest node down the smallest branch.
	//
	// In retrospect, I have two questions.
	// First, is there ever a circumstance in which the right branch will be
	// longer than the left?
	// Second, even if the branches do get out of balance, does the performance
	// gain that results from the balance every justify the cost of Size()?
	// We can always implement a cached value for Size(), but that still has a
	// memory cost.
	if a.Size() <= b.Size() {
		a.SiftDown(c)
	} else {
		b.SiftDown(c)
	}
}

func (n *Node) CheckBalance() {
	if n == nil {
		return
	}
	sizeLt := 0
	sizeRt := 0

	if n.Left != nil {
		sizeLt = n.Left.Size()
	}
	if n.Right != nil {
		sizeRt = n.Right.Size()
	}
	if sizeLt < sizeRt {
		log.Print("Right is longer than left.")
	}
}
